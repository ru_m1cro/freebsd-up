#!/bin/sh

###
# Variabels
###
COPY_FILES="
/boot/loader.conf
/boot/modules/*
/etc/make.conf
/etc/rc.conf
/etc/src.conf
/etc/sysctl.conf
/etc/wpa_supplicant.conf
/etc/ssh/sshd_config
/etc/master.passwd
/etc/passwd
/etc/pwd.db
/etc/spwd.db
/etc/group
/etc/fstab
"
EXTRA_DATASETS="
/tmp
"
MERGE_DATASETS=""

NEED_CHANGE_MOUNTPOINT=1
NEED_DEBUG_SNAPHOTS=1
NEED_UPDATE_LOADER=1

FREEBSD_VERSION="13.0-CURRENT"
FREEBSD_REVISION="20d728b559178577869e50c7e3c1bf0ad24a750c"
#SNAPSHOTS_URL="http://ftp.freebsd.org/pub/FreeBSD/snapshots/amd64/amd64/${FREEBSD_VERSION}"
SNAPSHOTS_URL="https://artifact.ci.freebsd.org/snapshot/head/${FREEBSD_REVISION}/amd64/amd64/"
SNAPSHOTS_LIST="base kernel lib32"
SNAPSHOTS_LIST_DEBUG="base-dbg kernel-dbg lib32-dbg"
TIME=$(date +%Y%m%d%H%M)
TMP_DIR=$(mktemp -d /tmp/freebsd-up.XXXXXX) || exit 1

###
# Colors
###
BOLD=`tput md 2>/dev/null` || :
RED=`tput md AF 1 2>/dev/null` || :
GREEN=`tput md AF 2 2>/dev/null` || :
YELLOW=`tput md AF 3 2>/dev/null` || :
RESET=`tput me 2>/dev/null` || :

###
# Functions
###
info() { echo "${BOLD}INFO${RESET}: $@" >&2; }
warn() { echo "${YELLOW}WARNING${RESET}: $@" >&2; }
crit() { echo "${RED}CRITICAL${RESET}: $@" >&2; }
ok()   { echo "$@: ${GREEN}Ok${RESET}" >&2; }
nok()  { echo "$@: ${RED}False${RESET}" >&2; }

usage() {
	USAGE=1
cat << EOF
Usage: $0
 -d zfs dataset
 -m temprorary root mountpoint
 Example:
 $0 -d tank
EOF
}

download_snapshots() {
	info "Downloading snapshots"
	for SNAPSHOT in ${SNAPSHOTS_LIST}; do
		fetch -o ${TMP_DIR} ${SNAPSHOTS_URL}/${SNAPSHOT}.txz
	done
	if [ ${NEED_DEBUG_SNAPHOTS} ]; then
		info "Downloading debug snapshots"
		for SNAPSHOT_DEBUG in ${SNAPSHOTS_LIST_DEBUG}; do
			fetch -o ${TMP_DIR} ${SNAPSHOTS_URL}/${SNAPSHOT_DEBUG}.txz
		done
	fi
}

our_exit() {
	if [ ${USAGE} = 0 ]; then
		info "Cleanup"
		rm -rf ${TMP_DIR}
	fi
	exit 0
}

###
# Code
###
trap our_exit EXIT 1 2 13 15

[ $# -le 0 ] && { usage ; exit 1; }

if [ "$(id -u)" != "0" ]; then
	crit "You should be a root!"
	exit 1
fi

ZFS_DATASET=${ZFS_DATASET:=z/root}

download_snapshots

info "Creating datasets"
zfs create -o mountpoint=/mnt/freebsd-upT${TIME} ${ZFS_DATASET}/freebsd-upT${TIME}
if [ "${EXTRA_DATASETS}" ]; then
	info "Creating extra datasets"
	for DATASET in ${EXTRA_DATASETS}; do
		zfs create -o mountpoint=/mnt/freebsd-upT${TIME}${DATASET} ${ZFS_DATASET}/freebsd-upT${TIME}${DATASET}
	done
fi
if [ "${MERGE_DATASETS}" ]; then
	info "Create datasets to merge"
	for DATASET in ${MERGE_DATASETS}; do
		zfs create -o mountpoint=/mnt/freebsd-upT${TIME}${DATASET} ${ZFS_DATASET}/freebsd-upT${TIME}${DATASET}
		info "Copy data for ${DATASET}"
		cp -pR ${DATASET}/* /mnt/freebsd-upT${TIME}${DATASET}
	done
fi

info "Extracting snapshots"
for SNAPSHOT in ${SNAPSHOTS_LIST}; do
	tar -x -C /mnt/freebsd-upT${TIME} -f ${TMP_DIR}/${SNAPSHOT}.txz
done
if [ "${NEED_DEBUG_SNAPHOTS}" ]; then
	info "Extracting debug snapshots"
	for SNAPSHOT_DEBUG in ${SNAPSHOTS_LIST_DEBUG}; do
		tar -x -C /mnt/freebsd-upT${TIME} -f ${TMP_DIR}/${SNAPSHOT_DEBUG}.txz
	done
fi

info "Copy our configuration/data files"
for FILE in ${COPY_FILES}; do
	cp -p ${FILE} /mnt/freebsd-upT${TIME}${FILE}
done

if [ "${NEED_CHANGE_MOUNTPOINT}" ]; then
	info "Changing mountpoints"
fi

if [ "${NEED_UPDATE_LOADER}" ]; then
	info "Update loader"
	bectl activate -t freebsd-upT${TIME}
fi

info "Done. Ready for reboot"
